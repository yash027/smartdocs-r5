import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { RouteService } from '../app-services/route.service';
import { ToastCtrlService } from '../app-services/alertCtrl.service';
import { StoreService } from '../app-services/store.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  loggedUserDetails: any = {};

  constructor(private alertController: AlertController,
              private store: StoreService,
              private toastController: ToastCtrlService,
              private router: RouteService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.loggedUserDetails = {};
    this.loggedUserDetails['firstName'] = localStorage.getItem('user_firstName');
    this.loggedUserDetails['lastName'] = localStorage.getItem('user_lastName');
    this.loggedUserDetails['email'] = localStorage.getItem('user_email');
  }

  onLogout() {
    this.presentLogoutAlert();
  }

  async presentLogoutAlert() {
    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Are you sure, you want to Logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Okay',
          handler: () => {
            this.userLoggedOutSuccessfully();
            this.router.routeTo('login');
            this.toastController.displayToastMessage('You have logged out successfully',1000);
          }
        }
      ]
    });

    await alert.present();
  }

  userLoggedOutSuccessfully() {
    sessionStorage.clear();
    localStorage.clear();
    this.store.authenticationToken = '';
    this.store.selectedRequest = '';
  }

}
