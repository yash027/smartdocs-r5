import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class StorageService {

    ifUserPresent: Subject<any>;

    constructor(private storage: Storage) {
        this.ifUserPresent = new Subject<any>();
    }

    addUser(user, key): Promise<any> {
        return this.storage.get(key).then( data => {
            if(data) {
                data.push(user);
                return this.storage.set(key, data);
            } else {
                return this.storage.set(key, [user]);
            }
        });
    }

    deleteUser(key): Promise<any> {
        return this.storage.get(key).then( data => {
            if ( !data || data.length === 0) {
                return null;
            }
            this.storage.set(key, []);
        });
    }

    getUser(key): Promise<any>{
        return this.storage.get(key).then( response => {
            return response;
        });
    }

    getStorageKeys(): Promise<any> {
        return this.storage.length().then( length => {
            const userData = {};
            if (length !== 0) {
                this.storage.forEach( (value, key) => {
                    userData['key'] = key;
                    userData['value'] = value;
                    this.ifUserPresent.next(userData);
                });
            } else {
                this.ifUserPresent.next(false);
            }
        })
    }

}