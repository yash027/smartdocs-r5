import { Injectable } from '@angular/core';
import { RestcallsService } from './restcalls.service';
import { routes } from 'src/environments/routes';
import { StorageService } from './storage.service';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  authenticationToken: string;

  loggedUserName: any;

  loader: any;

  tenantDetails: any = {};

  // Request Initiation Page Selected Request Object
  selectedRequest: string;

  // Inbox Request Approval Page Selected Request Object
  selectedFormRequest: string;

  cardColors: string[] = [
    'success',
    'primary',
    'secondary',
    'tertiary',
    'warning',
    'danger'
  ];

  setItemIntoSessionStorage(key: string, value: string) {
    sessionStorage.setItem(key, value);
  }

  setItemIntoLocalStorage(key: string, value: string) {
    localStorage.setItem(key, value);
  }

  constructor(
    private http: RestcallsService,
    private storage: StorageService,
    private loadingController: LoadingController
  ) {}

  provideUniqueCardColor() {
    const index =
      Math.floor(Math.random() * (this.cardColors.length - 1 - 0 + 1)) + 0;
    return this.cardColors[index];
  }

  saveUserDetails() {
    this.http
      .call_GET(routes.BASEHREF + routes.GET_ACCOUNT_DETAILS)
      .subscribe((response: any) => {
        this.loggedUserName = response.login;

        this.setItemIntoSessionStorage('user_firstName', response.firstName);
        this.setItemIntoSessionStorage('user_lastName', response.lastName);
        this.setItemIntoSessionStorage('user_email', response.email);
        this.setItemIntoSessionStorage('user_userName', response.login);

        this.setItemIntoLocalStorage('user_firstName', response.firstName);
        this.setItemIntoLocalStorage('user_lastName', response.lastName);
        this.setItemIntoLocalStorage('user_email', response.email);
        this.setItemIntoLocalStorage('user_userName', response.login);
      });
  }

  dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    let byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0){
        byteString = atob(dataURI.split(',')[1]);
    } else {
        byteString = unescape(dataURI.split(',')[1]);
    }
    // separate out the mime component
    const mimeString = dataURI
      .split(',')[0]
      .split(':')[1]
      .split(';')[0];

    // write the bytes of the string to a typed array
    const ia = new Uint8Array(byteString.length);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString });
  }

  async presentLoader(choice) {
    if (choice) {
      this.loader = await this.loadingController.create({
        message: 'Please wait!',
        duration: 2000
      });
      this.loader.present();
    }
  }

  getSAPDate(date) {
    let selectedDate: any = new Date(date);
    let month = selectedDate.getMonth();
    if (month === 9) {
      month = month + 1;
    }
    if (month < 9){
      month = month + 1;
      month = '0' + (month.toString());
    }
    let newDate = selectedDate.getDate();
    if (newDate <= 9) {
        newDate = '0' + (newDate.toString());
    }
    return selectedDate = selectedDate.getFullYear().toString() + month + newDate;
  }
}
