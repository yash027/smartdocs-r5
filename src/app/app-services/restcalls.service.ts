import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// const APIEndPoint = 'https://test-provisioning.azurewebsites.net/api/';

@Injectable({
  providedIn: 'root'
})

export class RestcallsService {
  constructor(private http: HttpClient) {}
  call_GET(url) {
    return this.http.get(url);
  }
  call_POST(url, data) {
    return this.http.post(url, data);
  }
  call_PUT(url, data) {
    return this.http.put(url, data);
  }
  call_DELETE(url) {
    return this.http.delete(url);
  }
}
