export const globals = {

    authentication_token: '',

    setItemIntoSessionStorage: (key: string, value: string) => {
        sessionStorage.setItem(key, value);
    },

    card_colors: ['success', 'dark', 'primary', 'secondary', 'tertiary', 'warning', 'danger'],

    isLoggedIn: false,

}
