import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})

export class ToastCtrlService {

    constructor(private toastController: ToastController) {

    }

    async displayToastMessage(alertMsg: string, alertDuration: number) {
        const toast = await this.toastController.create({
            message: alertMsg,
            duration: alertDuration
        });
        toast.present();
    }

}
