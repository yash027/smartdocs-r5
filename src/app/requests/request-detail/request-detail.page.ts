import {
  Component,
  OnInit,
  ChangeDetectorRef,
  AfterViewChecked,
  Output,
  EventEmitter
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { RequestDetailService } from './request-detail.service';
import { RouteService } from 'src/app/app-services/route.service';
import { StoreService } from 'src/app/app-services/store.service';
import { AlertController, Platform } from '@ionic/angular';
import { ToastCtrlService } from 'src/app/app-services/alertCtrl.service';

import { Plugins, Capacitor, CameraSource, CameraResultType } from '@capacitor/core';


@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.page.html',
  styleUrls: ['./request-detail.page.scss']
})
export class RequestDetailPage implements OnInit, AfterViewChecked {

  formId: string;
  formData: any = [];

  header: any = {};
  lineItems: any = [];
  lineItemValues: any = [];
  requestDescription: any;

  uploadedFiles: any = [];
  selectedImages: any = [];
  imagesToUpload: any = [];

  isMobilePlatform: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private service: RequestDetailService,
    private router: RouteService,
    private changeDetector: ChangeDetectorRef,
    private alertController: AlertController,
    private toastController: ToastCtrlService,
    private store: StoreService,
    private platform: Platform
  ) {}


  ionViewWillEnter() {
    this.isMobilePlatform = this.platform.is('mobile');
    this.service.selectedListValue.subscribe(this.ifSelectListValueForHeader);
    this.activatedRoute.paramMap.subscribe(param => {
      if (!this.store.selectedRequest){
        this.router.routeTo('/tabs/requests');
        this.toastController.displayToastMessage('You have to select a request first!', 1000);
      }
      this.formId = param.get('request');
      this.getFormData(this.formId);
    });
  }

  ionViewWillLeave() {
  }

  ngOnInit() {}

  getFormData(formId: string) {
    this.service.getFormDetails(formId).subscribe(
      response => {
        this.formData = response;
      },
      error => {
        this.router.routeTo('/tabs/requests');
      }
    );
  }

  onSelectSearchField(formField, sender) {
    this.service.showRequestInitiationModal(formField, sender);
  }

  ngAfterViewChecked() {
    this.changeDetector.detectChanges();
  }

  ifSelectListValueForHeader = value => {
    if (value.sendFrom === 'header') {
      this.header[value.requestObj.name] = value.selectedValue;
    } else if (value.sendFrom.sender === 'lineItem') {
      const index = value.sendFrom.lineItemIndex;
      const lineItem = this.lineItems[index];
      lineItem.forEach((element, i) => {
        if (element.name === value.requestObj.name) {
          const requestLineItemIndex = i;
          this.lineItems[index][requestLineItemIndex].value = value.selectedValue;
          return;
        }
      });
    }
  };

  addLineItem() {
    const lineItemFields = JSON.parse(JSON.stringify(this.formData.lineItemFields));
    lineItemFields['show'] = true;
    this.lineItems.push(lineItemFields);
  }

  showLineItem(item) {
    item.show = !item.show;
  }

  removeLineItem(index) {
    this.lineItems.splice(index, 1);
  }

  onSelectFile(event) {
    this.uploadedFiles = event.target.files;
    console.log(this.uploadedFiles);
  }

  submitRequest() {
    const finalListItems = [];
    this.lineItems.forEach( (element, i) => {
      const lineItemObj = {};
      const headerKeys = {};
      lineItemObj['sno'] = (i + 1);
      lineItemObj['subLineItems'] = null;
      element.forEach((h) => {
          headerKeys[h.name] = h.value;
      });
      lineItemObj['lineItems'] = headerKeys;
      finalListItems.push(lineItemObj);
    });
    const finalRequestObj = {};
    finalRequestObj['docType'] = this.store.selectedRequest;
    finalRequestObj['description'] = this.requestDescription;
    for (const key in this.header) {
      if (this.header[key]) {
        this.formData.fields.forEach(element => {
          if (key === element.name) {
            if (element.type === 'date') {
              this.header[key] = this.store.getSAPDate(this.header[key]);
            }
          }
        });
      }
    }
    finalRequestObj['header'] = this.header;
    finalRequestObj['lineItems'] = finalListItems;
    this.service.sendRequestInitiation(finalRequestObj, [this.uploadedFiles, this.imagesToUpload], null).subscribe( response => {
      this.showSuccessfulAlert(response);
      this.destructureAllFields();
    });
  }

  async showSuccessfulAlert(data) {
    const alert = await this.alertController.create({
      header: 'Request Initiation Successful',
      message: 'You have successfully initiated the Request.<br>Created By: ' + data.createdBy,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.router.routeTo('/tabs/requests');
          }
        }
      ]
    });

    await alert.present();
  }

  onPickImage() {
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.toastController.displayToastMessage('Unable To Open Camera', 1000);
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 80,
      source: CameraSource.Camera,
      correctOrientation: true,
      height: 100,
      width: 100,
      resultType: CameraResultType.DataUrl
    }).then( image => {
      const img = image.dataUrl;
      this.selectedImages.push(img);
      const blobimg = this.store.dataURItoBlob(image.dataUrl);
      this.imagesToUpload.push(blobimg);
    }).catch( error => {
      console.log(error);
      return false;
    });
  }

  destructureAllFields() {
    this.formId = null;
    this.formData.length = 0;
    this.header = {};
    this.lineItems.length = 0;
    this.lineItemValues.length = 0;
    this.requestDescription = null;
    this.uploadedFiles.length = 0;
    this.selectedImages.length = 0;
    this.imagesToUpload.length = 0;
  }

}
