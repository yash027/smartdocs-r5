import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RequestDetailPage } from './request-detail.page';
import { RequestDetailService } from './request-detail.service';

import { MatButtonModule,  MatStepperModule, MatExpansionModule} from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: RequestDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatStepperModule,
    MatExpansionModule,
  ],
  declarations: [RequestDetailPage],
  providers: [
    RequestDetailService
  ]
})
export class RequestDetailPageModule {}
