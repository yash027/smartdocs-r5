import { Injectable } from '@angular/core';
import { RestcallsService } from 'src/app/app-services/restcalls.service';
import { routes } from 'src/environments/routes';
import { ModalController } from '@ionic/angular';
import { RequestInitiationModalPage } from 'src/app/modals/request-initiation-modal/request-initiation-modal.page';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestDetailService {
  selectedListValue: Subject<any>;

  constructor(
    private http: RestcallsService,
    private modalController: ModalController
  ) {
    this.selectedListValue = new Subject<any>();
  }

  getFormDetails(formId: string) {
    return this.http.call_GET(
      routes.BASEHREF + routes.GET_FORMS_DETAILS + '/' + formId
    );
  }

  async showRequestInitiationModal(modalProps: any, senderObj) {
    const modal = await this.modalController.create({
      component: RequestInitiationModalPage,
      componentProps: {
        requestData: modalProps
      }
    });

    modal.onDidDismiss().then(data => {
      this.selectedListValue.next({
        selectedValue: data.data,
        requestObj: modalProps,
        sendFrom: senderObj
      });
    });

    return await modal.present();
  }

  sendRequestInitiation(
    finalRequestObj: any,
    uploadedFiles: any,
    actionValue: any
  ) {
    let form = new FormData();
    uploadedFiles.forEach(element => {
      for (const key in element) {
        if (element[key]) {
          form.append('files', element[key]);
        }
      }
    });
    const encodedUrl = '?requestData=' + encodeURIComponent(JSON.stringify(finalRequestObj)) +
      '&action=' + actionValue;
    return this.http.call_POST(routes.BASEHREF + routes.SAVE_REQUEST + encodedUrl, form);
  }
}
