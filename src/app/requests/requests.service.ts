import { Injectable } from '@angular/core';
import { RestcallsService } from '../app-services/restcalls.service';
import { routes } from 'src/environments/routes';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private http: RestcallsService) { }

  getRequests(): Observable<any> {
    return this.http.call_GET(routes.BASEHREF + routes.GET_REQUESTS_URL);
  }

}
