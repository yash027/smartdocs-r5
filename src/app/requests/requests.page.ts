import { Component, OnInit } from '@angular/core';
import { RequestsService } from './requests.service';
import { StoreService } from '../app-services/store.service';
import { Router } from '@angular/router';
import { StorageService } from '../app-services/storage.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.page.html',
  styleUrls: ['./requests.page.scss'],
})
export class RequestsPage implements OnInit {

  userRequests: any = [];

  constructor(private requestsService: RequestsService, private store: StoreService, private storage: StorageService) { }

  ngOnInit() {
  }

  ionViewWillLeave() {
    this.userRequests = [];
  }

  ionViewWillEnter() {
    if (this.store.loader) {
      this.store.presentLoader(false);
    }
    this.userRequests.length = 0;
    this.storage.getStorageKeys();
    this.requestsService.getRequests().subscribe( response => {
      response.forEach( (element) => {
        // Setting card background property within response
        element.background = this.store.provideUniqueCardColor();
      });
      this.userRequests = response;
    });
  }

  getRequestDetails(request) {
    this.store.selectedRequest = request.id;
  }

}
