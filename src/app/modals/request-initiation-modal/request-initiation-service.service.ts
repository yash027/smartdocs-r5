import { Injectable } from '@angular/core';
import { RestcallsService } from 'src/app/app-services/restcalls.service';
import { routes } from 'src/environments/routes';

@Injectable({
  providedIn: 'root'
})
export class RequestInitiationService {

  constructor(private restCallService: RestcallsService) { }

  getListData(listName: string) {
    return this.restCallService.call_GET(routes.BASEHREF + routes.GET_LIST_DATA + '/' + listName);
  }
}
