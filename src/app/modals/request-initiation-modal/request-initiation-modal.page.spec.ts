import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestInitiationModalPage } from './request-initiation-modal.page';

describe('RequestInitiationModalPage', () => {
  let component: RequestInitiationModalPage;
  let fixture: ComponentFixture<RequestInitiationModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestInitiationModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestInitiationModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
