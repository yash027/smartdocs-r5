import { TestBed } from '@angular/core/testing';

import { RequestInitiationService } from './request-initiation-service.service';

describe('RequestInitiationServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestInitiationService = TestBed.get(RequestInitiationService);
    expect(service).toBeTruthy();
  });
});
