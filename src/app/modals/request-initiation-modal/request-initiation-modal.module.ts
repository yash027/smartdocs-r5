import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { MatTableModule } from '@angular/material';

import { RequestInitiationModalPage } from './request-initiation-modal.page';

const routes: Routes = [
  {
    path: '',
    component: RequestInitiationModalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatTableModule
  ],
  declarations: [RequestInitiationModalPage]
})
export class RequestInitiationModalPageModule {}
