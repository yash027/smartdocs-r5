import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { RequestInitiationService } from './request-initiation-service.service';

@Component({
  selector: 'app-request-initiation-modal',
  templateUrl: './request-initiation-modal.page.html',
  styleUrls: ['./request-initiation-modal.page.scss'],
})
export class RequestInitiationModalPage implements OnInit, OnDestroy {

  title: string;

  requestData: any;

  tableDataSource: any = [];

  displayedColumns: any = [];

  constructor(private navParams: NavParams, private service: RequestInitiationService, private modalController: ModalController) { }

  ngOnInit() {
    if(this.navParams.get('requestData')){
      this.requestData = this.navParams.get('requestData');
      this.title = this.requestData.label;
      this.getListData(this.requestData.listName);
      this.requestData.listFields.forEach(element => {
        this.displayedColumns.push(element.name);
      });
    }
  }

  getListData(listName: string) {
    this.service.getListData(listName).subscribe( response => {
      this.tableDataSource = response;
    });
  }

  getSelectedListItem(row) {
    const fieldName = this.requestData.outputField;
    const requiredValue = row[fieldName];
    this.modalController.dismiss(requiredValue);
  }

  ngOnDestroy() {
    this.title = undefined;
    this.requestData = undefined;
    this.tableDataSource = undefined;
    this.displayedColumns = undefined;
  }

}
