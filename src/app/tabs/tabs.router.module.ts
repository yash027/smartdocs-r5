import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';
import { AuthGuardService } from '../app-services/auth-guards/auth-guard.service';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'requests',
        children: [
          {
            path: '',
            loadChildren: './../requests/requests.module#RequestsPageModule',
            canActivate: [AuthGuardService]
          },
          {
            path: ':request',
            loadChildren: './../requests/request-detail/request-detail.module#RequestDetailPageModule',
            canActivate: [AuthGuardService]
          }
        ]
      },
      {
        path: 'outbox',
        children: [
          {
            path: '',
            loadChildren: './../outbox/outbox.module#OutboxPageModule',
            canActivate: [AuthGuardService]
          },
          {
            path: ':outboxRequestDetail',
            children: [
              {
                path: '',
                loadChildren: './../outbox/outbox-detail/outbox-detail.module#OutboxDetailPageModule',
                canActivate: [AuthGuardService]
              },
              {
                path: ':outbox-form-details',
                loadChildren: './../outbox/outbox-detail/outbox-form-detail/outbox-form-detail.module#OutboxFormDetailPageModule',
                canActivate: [AuthGuardService]
              }
            ]
          }
        ]
      },
      {
        path: 'inbox',
        children: [
          {
            path: '',
            loadChildren: './../inbox/inbox.module#InboxPageModule',
            canActivate: [AuthGuardService]
          },
          {
            path: ':inboxRequestDetail',
            children: [
              {
                path: '',
                loadChildren: './../inbox/inbox-detail/inbox-detail.module#InboxDetailPageModule',
                canActivate: [AuthGuardService]
              },
              { path: ':request-form-details',
                loadChildren: './../inbox/inbox-detail/request-form-details/request-form-details.module#RequestFormDetailsPageModule',
                canActivate: [AuthGuardService]
              }
            ],
          },
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: './../profile/profile.module#ProfilePageModule',
            canActivate: [AuthGuardService]
          }
        ]
      },
      {
        path: '',
        redirectTo: './../requests/requests.module#RequestsPageModule',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}

// const routes: Routes = [
//   {
//     path: 'tabs',
//     component: TabsPage,
//     children: [
//       {
//         path: 'requests',
//         children: [
//           {
//             path: '',
//             loadChildren: './../requests/requests.module#RequestsPageModule',
//             canActivate: [AuthGuardService]
//           },
//           {
//             path: ':request',
//             loadChildren: './../requests/request-detail/request-detail.module#RequestDetailPageModule',
//             canActivate: [AuthGuardService]
//           }
//         ]
//       },
//       {
//         path: 'inbox',
//         children: [
//           {
//             path: '',
//             loadChildren: './../inbox/inbox.module#InboxPageModule',
//             canActivate: [AuthGuardService]
//           }
//         ]
//       },
//       {
//         path: 'profile',
//         children: [
//           {
//             path: '',
//             loadChildren: './../profile/profile.module#ProfilePageModule',
//             canActivate: [AuthGuardService]
//           }
//         ]
//       },
//       {
//         path: '',
//         children: [
//           {
//             path: '',
//             loadChildren: './../requests/requests.module#RequestsPageModule',
//             canActivate: [AuthGuardService]
//           }
//         ]
//       }
//     ]
//   }
// ];
