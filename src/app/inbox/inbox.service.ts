import { Injectable } from '@angular/core';
import { RestcallsService } from '../app-services/restcalls.service';
import { routes } from 'src/environments/routes';

@Injectable({
  providedIn: 'root'
})
export class InboxService {

  constructor(private http: RestcallsService) { }

  getInboxData() {
    return this.http.call_GET(routes.BASEHREF + routes.GET_INBOX_DATA);
  }
}
