import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InboxDetailPage } from './inbox-detail.page';

describe('InboxDetailPage', () => {
  let component: InboxDetailPage;
  let fixture: ComponentFixture<InboxDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InboxDetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InboxDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
