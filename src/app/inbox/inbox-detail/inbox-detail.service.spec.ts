import { TestBed } from '@angular/core/testing';

import { InboxDetailService } from './inbox-detail.service';

describe('InboxDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InboxDetailService = TestBed.get(InboxDetailService);
    expect(service).toBeTruthy();
  });
});
