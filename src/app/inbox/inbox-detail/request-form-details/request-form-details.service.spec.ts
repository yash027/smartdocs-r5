import { TestBed } from '@angular/core/testing';

import { RequestFormDetailsService } from './request-form-details.service';

describe('RequestFormDetailsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestFormDetailsService = TestBed.get(RequestFormDetailsService);
    expect(service).toBeTruthy();
  });
});
