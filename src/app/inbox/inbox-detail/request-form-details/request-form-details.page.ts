import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RequestFormDetailsService } from './request-form-details.service';
import { RouteService } from 'src/app/app-services/route.service';
import { Plugins, Capacitor, CameraSource, CameraResultType } from '@capacitor/core';
import { ToastCtrlService } from 'src/app/app-services/alertCtrl.service';
import { StoreService } from 'src/app/app-services/store.service';
import { Platform, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-request-form-details',
  templateUrl: './request-form-details.page.html',
  styleUrls: ['./request-form-details.page.scss']
})
export class RequestFormDetailsPage implements OnInit {
  /* tslint:disable:no-string-literal */
  requestFormName: any;
  requestName: any;

  entireFormRequestData: any;

  lineItems: any = [];

  uploadedFiles: any = [];
  selectedImages: any = [];

  imagesToUpload: any = [];

  isMobilePlatform: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: RouteService,
    private service: RequestFormDetailsService,
    private toastController: ToastCtrlService,
    private alertController: AlertController,
    private store: StoreService,
    private platform: Platform
  ) {}

  ngOnInit() {
    this.isMobilePlatform = this.platform.is('mobile');
    this.requestFormName = '';
    this.requestFormName = '';
    this.entireFormRequestData = {};
    this.lineItems = [];
    this.activatedRoute.paramMap.subscribe(param => {
      this.requestFormName = param.get('request-form-details');
      this.requestName = param.get('inboxRequestDetail');
      this.getRequestFormData([this.requestName, this.requestFormName]);
    });
  }

  getRequestFormData(requestFormData) {
    this.service
      .getRequestFormData(requestFormData)
      .subscribe((response: any) => {
        this.entireFormRequestData = response;
        if (response.formId) {
          this.service
            .getEntireFormData(response.formId)
            .subscribe(formResponse => {
              this.entireFormRequestData['formResponse'] = formResponse;
              if (this.entireFormRequestData.lineItems) {
                this.setUpLineItems(this.entireFormRequestData);
              }
            });
        } else {
          this.backToInbox();
        }
      });
  }

  backToInbox() {
    this.router.routeTo('/tabs/inbox');
  }

  setUpLineItems(formData) {
    formData.lineItems.forEach(element => {
      element['lineItemFields'] = formData.formResponse.lineItemFields;
    });
  }

  deleteLineItemFields(formData) {
    formData.lineItems.forEach(element => {
      delete element.lineItemFields;
    });
  }

  processForm(activity, comment) {
    this.deleteLineItemFields(this.entireFormRequestData);
    const json = {};
    json['activities'] = [activity];
    json['comment'] = comment;
    json['header'] = this.entireFormRequestData.header;
    json['lineItems'] = this.entireFormRequestData.lineItems;
    this.service.processItem(this.requestName,
                             this.requestFormName, json,
                             [this.imagesToUpload,
                              this.uploadedFiles,
                              this.entireFormRequestData.attachments])
                             .subscribe( response => {
      console.log(response);
      this.showSuccessfulAlert(activity);
    });
  }

  onFileDownload(file) {
    this.service
      .getDownloadFile(this.requestFormName, file.archiveId, file.archiveDocId)
      .subscribe(response => {
        const keys = response.headers.keys();
        const headers = {};
        keys.forEach( element => {
          headers[element] = response.headers.get(element);
        });
        const data = response.body;
        const fileName = file.fileName;
        const contentType = headers['content-type'];
        const linkElement = document.createElement('a');
        try {
          const blob = new Blob([data], { type: contentType });
          const url = window.URL.createObjectURL(blob);
          linkElement.setAttribute('href', url);
          linkElement.setAttribute('download', fileName);
          const clickEvent = new MouseEvent('click', {
            view: window,
            bubbles: true,
            cancelable: false
          });
          linkElement.dispatchEvent(clickEvent);
        } catch (ex) {
        }
      });
  }

  onSelectFile(event) {
    this.uploadedFiles = event.target.files;
  }

  onPickImage() {
    if (!Capacitor.isPluginAvailable('Camera')) {
      this.toastController.displayToastMessage('Unable To Open Camera', 1000);
      return;
    }
    Plugins.Camera.getPhoto({
      quality: 80,
      source: CameraSource.Camera,
      correctOrientation: true,
      height: 100,
      width: 100,
      resultType: CameraResultType.DataUrl
    }).then( image => {
      const img = image.dataUrl;
      this.selectedImages.push(img);
      const blobimg = this.store.dataURItoBlob(image.dataUrl);
      this.imagesToUpload.push(blobimg);
    }).catch( error => {
      console.log(error);
      return false;
    });
  }

  async showCommentAlert(activity) {
    const alert = await this.alertController.create({
      header: 'Add Comment',
      inputs: [
        {
          type: 'text',
          name: 'submissionComment',
          placeholder: 'Add Comments'
        }
      ],
      buttons: [
        {
          text: 'Okay',
          handler: (data) => {
            this.processForm(activity, data.submissionComment);
          }
        }
      ]
    });

    await alert.present();
  }

  async showSuccessfulAlert(activity) {
    const alert = await this.alertController.create({
      header: 'Request Initiation Successful',
      message: 'You have successfully ' + activity.description + 'd The Request.',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.router.routeTo('/tabs/inbox');
          }
        }
      ]
    });

    await alert.present();
  }
}
