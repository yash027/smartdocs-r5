import { Injectable } from '@angular/core';
import { RestcallsService } from 'src/app/app-services/restcalls.service';
import { routes } from 'src/environments/routes';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestFormDetailsService {
  constructor(private http: RestcallsService, private httpClient: HttpClient) {}

  getRequestFormData(formRequest) {
    return this.http.call_GET(
      routes.BASEHREF +
        routes.GET_INBOX_DATA +
        routes.GET_REQUEST_DETAIL +
        '/' +
        formRequest[0] +
        routes.SAVE_REQUEST +
        '/' +
        formRequest[1]
    );
  }

  getEntireFormData(formId) {
    return this.http.call_GET(
      routes.BASEHREF + routes.GET_FORMS_DETAILS + '/' + formId
    );
  }

  getDownloadFile(reqId, archiveId, archiveDocId) {
    const appUrl =
      '?archiveId=' +
      archiveId +
      '&requestId=' +
      reqId +
      '&archiveDocId=' +
      archiveDocId +
      '&attachment=true';
    return this.httpClient.get(
      routes.BASEHREF + routes.GET_INBOX_DATA + routes.DOWNLOAD_FILE + appUrl,
      { headers: { Accept: '*/*' }, responseType: 'blob', observe: 'response' }
    );
  }

  processItem(requestName, requestFormName, jsonData, form) {
    const appUrl = routes.BASEHREF + routes.GET_INBOX_DATA
    + routes.GET_REQUEST_DETAIL + '/' + requestName
    + routes.SAVE_REQUEST + '/' + requestFormName + '/submit';
    const formData = new FormData();
    const encoded = '?requestData=' + encodeURIComponent(JSON.stringify(jsonData));
    form.forEach(element => {
      if (element) {
        if (Array.isArray(element)) {
          element.forEach( subElement => {
            for (const key in subElement) {
              if (subElement[key]) {
                formData.append('files', subElement[key]);
              }
            }
          });
        } else {
          for (const key in element){
            if (element[key]) {
              formData.append('files', element[key]);
            }
          }
        }
      }
    });
    return this.http.call_POST(appUrl + encoded, formData);
  }
}