import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InboxDetailService } from './inbox-detail.service';
import { StoreService } from 'src/app/app-services/store.service';

@Component({
  selector: 'app-inbox-detail',
  templateUrl: './inbox-detail.page.html',
  styleUrls: ['./inbox-detail.page.scss'],
})
export class InboxDetailPage implements OnInit {

  requestName: any;
  requestsDetail: any = [];

  constructor(private activatedRoute: ActivatedRoute, private service: InboxDetailService, private store: StoreService) { }

  ngOnInit() {
    this.requestName = '';
    this.requestsDetail.length = 0;
    this.activatedRoute.paramMap.subscribe(param => {
      this.requestName = param.get('inboxRequestDetail');
      this.store.selectedFormRequest = this.requestName;
      this.getRequestData(this.requestName);
    });
  }

  getRequestData(requestName) {
    this.service.getRequestData(requestName).subscribe( response => {
      this.requestsDetail = response;
      this.requestsDetail.forEach(element => {
        element['color'] = this.store.provideUniqueCardColor();
      });
    })
  }

}
