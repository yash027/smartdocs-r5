import { Component, OnInit } from '@angular/core';
import { InboxService } from './inbox.service';
import { StoreService } from '../app-services/store.service';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.page.html',
  styleUrls: ['./inbox.page.scss'],
})
export class InboxPage implements OnInit {

  requestsInboxData: any = [];

  constructor(private service: InboxService, private store: StoreService) { }

  ionViewWillEnter() {
    this.requestsInboxData.length = 0;
    this.getInboxData();
  }

  ionViewWillLeave() {
    this.requestsInboxData.length = 0;
  }

  ngOnInit() {}

  getInboxData() {
    this.service.getInboxData().subscribe( response => {
      this.requestsInboxData = response;
      this.requestsInboxData.forEach(element => {
        element.color = this.store.provideUniqueCardColor();
      });
    });
  }

}
