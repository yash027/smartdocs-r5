import { Injectable } from '@angular/core';
import { routes } from 'src/environments/routes';
import { RestcallsService } from '../app-services/restcalls.service';

@Injectable({
  providedIn: 'root'
})
export class OutboxService {

  constructor(private http: RestcallsService) { }

  getOutboxData() {
    return this.http.call_GET(routes.BASEHREF + routes.GET_OUTBOX_DATA);
  }
}
