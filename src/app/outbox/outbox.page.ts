import { Component, OnInit } from '@angular/core';
import { OutboxService } from './outbox.service';
import { StoreService } from '../app-services/store.service';

@Component({
  selector: 'app-outbox',
  templateUrl: './outbox.page.html',
  styleUrls: ['./outbox.page.scss'],
})
export class OutboxPage implements OnInit {

  requestsOutboxData: any = [];

  constructor(private service: OutboxService, private store: StoreService) { }

  ngOnInit() {  }

  getOutboxData() {
    this.requestsOutboxData.length = 0;
    this.service.getOutboxData().subscribe( response => {
      this.requestsOutboxData = response;
      this.requestsOutboxData.forEach(element => {
        element.color = this.store.provideUniqueCardColor();
      });
    });
  }

  ionViewWillEnter() {
    this.getOutboxData();
  }

  ionViewWillLeave() {
    this.requestsOutboxData.length = 0;
  }

}
