import { TestBed } from '@angular/core/testing';

import { OutboxService } from './outbox.service';

describe('OutboxService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutboxService = TestBed.get(OutboxService);
    expect(service).toBeTruthy();
  });
});
