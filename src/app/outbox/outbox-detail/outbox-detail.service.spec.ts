import { TestBed } from '@angular/core/testing';

import { OutboxDetailService } from './outbox-detail.service';

describe('OutboxDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutboxDetailService = TestBed.get(OutboxDetailService);
    expect(service).toBeTruthy();
  });
});
