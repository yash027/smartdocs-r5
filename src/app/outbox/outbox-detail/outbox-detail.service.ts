import { Injectable } from '@angular/core';
import { RestcallsService } from 'src/app/app-services/restcalls.service';
import { routes } from 'src/environments/routes';

@Injectable({
  providedIn: 'root'
})
export class OutboxDetailService {

  constructor(private http: RestcallsService) { }

  getRequestData(requestName) {
    return this.http.call_GET(routes.BASEHREF + routes.GET_OUTBOX_DATA
       + routes.GET_REQUEST_DETAIL + '/' + requestName);
  }
}
