import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OutboxDetailPage } from './outbox-detail.page';

import { MatExpansionModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: OutboxDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    MatExpansionModule
  ],
  declarations: [OutboxDetailPage]
})
export class OutboxDetailPageModule {}
