import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OutboxDetailService } from './outbox-detail.service';
import { StoreService } from 'src/app/app-services/store.service';

@Component({
  selector: 'app-outbox-detail',
  templateUrl: './outbox-detail.page.html',
  styleUrls: ['./outbox-detail.page.scss'],
})
export class OutboxDetailPage implements OnInit {

  requestName: any;
  requestsDetail: any = [];

  constructor(private activatedRoute: ActivatedRoute, private service: OutboxDetailService, private store: StoreService) { }

  ngOnInit() {
    this.requestName = '';
    this.requestsDetail.length = 0;
    this.activatedRoute.paramMap.subscribe(param => {
      this.requestName = param.get('outboxRequestDetail');
      this.store.selectedFormRequest = this.requestName;
      this.getRequestData(this.requestName);
    });
  }

  getRequestData(requestName) {
    this.service.getRequestData(requestName).subscribe( response => {
      this.requestsDetail = response;
      this.requestsDetail.forEach(element => {
        element['color'] = this.store.provideUniqueCardColor();
      });
    })
  }

}
