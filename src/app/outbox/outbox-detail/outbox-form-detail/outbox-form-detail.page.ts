import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteService } from 'src/app/app-services/route.service';
import { OutboxFormDetailService } from './outbox-form-detail.service';

@Component({
  selector: 'app-outbox-form-detail',
  templateUrl: './outbox-form-detail.page.html',
  styleUrls: ['./outbox-form-detail.page.scss'],
})
export class OutboxFormDetailPage implements OnInit {

  requestFormName: any;
  requestName: any;

  entireFormRequestData: any = {};

  lineItems: any = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: RouteService,
    private service: OutboxFormDetailService
  ) {}

  ngOnInit() {
    this.requestFormName = '';
    this.requestFormName = '';
    this.entireFormRequestData = {};
    this.lineItems = [];
    this.activatedRoute.paramMap.subscribe(param => {
      this.requestFormName = param.get('outbox-form-details');
      this.requestName = param.get('outboxRequestDetail');
      this.getRequestFormData([this.requestName, this.requestFormName]);
    });
  }

  getRequestFormData(requestFormData) {
    this.service
      .getRequestFormData(requestFormData)
      .subscribe((response: any) => {
        this.entireFormRequestData = response;
        if (response.formId) {
          this.service
            .getEntireFormData(response.formId)
            .subscribe(formResponse => {
              this.entireFormRequestData['formResponse'] = formResponse;
              if (this.entireFormRequestData.lineItems) {
                this.setUpLineItems(this.entireFormRequestData);
              }
            });
        } else {
          this.backToInbox();
        }
      });
  }

  backToInbox() {
    this.router.routeTo('/tabs/inbox');
  }

  setUpLineItems(formData) {
    formData.lineItems.forEach(element => {
      element['lineItemFields'] = formData.formResponse.lineItemFields;
    });
  }

  onFileDownload(file) {
    this.service
      .getDownloadFile(this.requestFormName, file.archiveId, file.archiveDocId)
      .subscribe(response => {
        const keys = response.headers.keys();
        const headers = {};
        keys.forEach( element => {
          headers[element] = response.headers.get(element);
        });
        const data = response.body;
        const fileName = file.fileName;
        const contentType = headers['content-type'];
        const linkElement = document.createElement('a');
        try {
          const blob = new Blob([data], { type: contentType });
          const url = window.URL.createObjectURL(blob);
          linkElement.setAttribute('href', url);
          linkElement.setAttribute('download', fileName);
          const clickEvent = new MouseEvent('click', {
            view: window,
            bubbles: true,
            cancelable: false
          });
          linkElement.dispatchEvent(clickEvent);
        } catch (ex) {
        }
      });
  }

}
