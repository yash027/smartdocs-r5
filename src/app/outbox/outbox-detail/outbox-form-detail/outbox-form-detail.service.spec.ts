import { TestBed } from '@angular/core/testing';

import { OutboxFormDetailService } from './outbox-form-detail.service';

describe('OutboxFormDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutboxFormDetailService = TestBed.get(OutboxFormDetailService);
    expect(service).toBeTruthy();
  });
});
