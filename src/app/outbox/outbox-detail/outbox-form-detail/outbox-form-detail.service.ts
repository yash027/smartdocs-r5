import { Injectable } from '@angular/core';
import { routes } from 'src/environments/routes';
import { RestcallsService } from 'src/app/app-services/restcalls.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OutboxFormDetailService {

  constructor(private http: RestcallsService, private httpClient: HttpClient) {}

  getRequestFormData(formRequest) {
    return this.http.call_GET(
      routes.BASEHREF +
        routes.GET_OUTBOX_DATA +
        routes.GET_REQUEST_DETAIL +
        '/' +
        formRequest[0] +
        routes.SAVE_REQUEST +
        '/' +
        formRequest[1]
    );
  }

  getEntireFormData(formId) {
    return this.http.call_GET(
      routes.BASEHREF + routes.GET_FORMS_DETAILS + '/' + formId
    );
  }

  getDownloadFile(reqId, archiveId, archiveDocId) {
    const appUrl =
      '?archiveId=' +
      archiveId +
      '&requestId=' +
      reqId +
      '&archiveDocId=' +
      archiveDocId +
      '&attachment=true';
    return this.httpClient.get(
      routes.BASEHREF + routes.GET_OUTBOX_DATA + routes.DOWNLOAD_FILE + appUrl,
      { headers: { Accept: '*/*' }, responseType: 'blob', observe: 'response' }
    );
  }
}
