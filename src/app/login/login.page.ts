import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { ToastCtrlService } from '../app-services/alertCtrl.service';
import { RouteService } from '../app-services/route.service';
import { StoreService } from '../app-services/store.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  credentials: any = {};

  loginImgSrc = './../../assets/logo-small.png';

  isAccountIdPresent = false;

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
 
  hideShowPassword() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  constructor(
    private loginService: LoginService,
    private router: RouteService,
    private toastController: ToastCtrlService,
    private store: StoreService
  ) {}

  ngOnInit() {
    this.credentials = {};
  }

  login() {
    this.store.presentLoader(true);
    this.loginService.login(this.credentials).subscribe(
      response => {
        this.toastController.displayToastMessage('You Have Successfully Logged In!', 1000);
        this.loginService.savingUserData(response);
        this.router.routeTo('tabs/requests');
      },
      (error: any) => {
        this.toastController.displayToastMessage('Login Failed! Please Try Again.', 2000);
      }
    );
  }

  getTenantUrl() {
    this.isAccountIdPresent = true;
    this.loginService.getTenantUrl(this.credentials.accountId).subscribe( (response: any) => {
      this.store.tenantDetails = response;
      console.log(this.store.tenantDetails.internalURL);
    });
  }

}
