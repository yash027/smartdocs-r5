import { Injectable } from '@angular/core';
import { RestcallsService } from '../app-services/restcalls.service';
import { Observable } from 'rxjs';
import { routes } from 'src/environments/routes';
import { StorageService } from '../app-services/storage.service';
import { StoreService } from '../app-services/store.service';
import { RouteService } from '../app-services/route.service';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: RestcallsService,
              private storage: StorageService,
              private storeService: StoreService,
              private router: RouteService,
              private httpClient: HttpClient
              ) {
    this.storage.ifUserPresent.subscribe(this.ifUserExist);
  }

  login(userDetails: any): Observable<any> {
    return this.http.call_POST(routes.BASEHREF + routes.LOGIN_URL, userDetails);
  }

  checkLoggedInUser() {
    this.storage.getStorageKeys().then();
  }

  ifUserExist = (value) => {
    if (value) {
      this.router.routeTo('tabs/requests');
    }
  }

  savingUserData(response) {
    this.storeService.authenticationToken = response.id_token;
    this.storeService.setItemIntoSessionStorage('accessToken', response.id_token);
    this.storeService.saveUserDetails();
  }

  getTenantUrl(accountId) {
    return this.httpClient.get(routes.GET_TENANT + accountId);
  }
}
