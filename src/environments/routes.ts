export const routes = {

    GET_TENANT: 'http://smartdocs-mobile-login.appspot.com/rest/api/getCompanyURL/',

    BASEHREF: 'https://epson.smartportal-be.smartdocs.ai/api',

    GET_ACCOUNT_DETAILS: '/account',

    // BASEHREF: 'https://himatsingka-test.smartdocsnow.com/smartportal-server/api',

    LOGIN_URL: '/authenticate',

    SAVE_REQUEST: '/requests',

    GET_REQUESTS_URL: '/requests/initiate',

    GET_FORMS_DETAILS: '/forms',

    GET_LIST_DATA: '/getdynamicinstace',

    GET_INBOX_DATA: '/requests/inbox',

    GET_REQUEST_DETAIL: '/requestType',

    GET_FORM_REQUEST_DETAIL: '/inbox/requestType',

    DOWNLOAD_FILE: '/filedownload',

    GET_OUTBOX_DATA: '/requests/outbox'

}